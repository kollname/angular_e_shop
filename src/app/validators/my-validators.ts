import {FormControl, ValidationErrors} from '@angular/forms';

export class MyValidators {

  static noWhiteSpaces(control: FormControl): ValidationErrors{

    if ((control.value != null) && (control.value.trim().length === 0)) {
      return {noWhiteSpaces: true};
    }else {
      return null;
    }
  }
}
