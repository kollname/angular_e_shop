import { Component, OnInit } from '@angular/core';
import {Product} from '../../common/product';
import {CartService} from '../../services/cart-service';

@Component({
  selector: 'app-cart-stat',
  templateUrl: './cart-stat.component.html',
  styleUrls: ['./cart-stat.component.css']
})
export class CartStatComponent implements OnInit {

  totalPrice = 0.00;
  totalQuantity = 0;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.updateCartStatus();
  }

  // tslint:disable-next-line:typedef
  updateCartStatus(){
    this.cartService.totalPrice.subscribe(
      data => this.totalPrice = data);

    this.cartService.totalQuantity.subscribe(
      data => this.totalQuantity = data);
  }
}
