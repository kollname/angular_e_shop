import { Component, OnInit } from '@angular/core';
import {Product} from '../../common/product';
import {ProductService} from '../../services/product.service';
import {ActivatedRoute} from '@angular/router';
import {CartItem} from '../../common/cart-item';
import {CartService} from '../../services/cart-service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list-grid.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Product[] = [];
  currentCategoryId = 1;
  previousCategoryId = 1;
  currentCategoryName: string;
  searchMode = false;
  pageNumber = 1;
  pageSize = 5;
  totalElements = 0;


  constructor(private productService: ProductService, private cartService: CartService, private route: ActivatedRoute) {
  }

  // tslint:disable-next-line:typedef
  ngOnInit(){
    this.route.paramMap.subscribe(() => {
      this.listProducts();
    });
  }

  // tslint:disable-next-line:typedef
  listProducts(){
    this.searchMode = this.route.snapshot.paramMap.has('keyword');
    if (this.searchMode){
      this.handleSearchProducts();
    }else {
      this.handleListProducts();
    }
  }

  // tslint:disable-next-line:typedef
  handleSearchProducts(){

    const theKeyword: string = this.route.snapshot.paramMap.get('keyword');

    this.productService.searchProducts(this.pageNumber - 1, this.pageSize, theKeyword).subscribe(this.processResult());
  }

  // tslint:disable-next-line:typedef
  handleListProducts(){
    const hasCategoryId: boolean = this.route.snapshot.paramMap.has('id');

    if (hasCategoryId){
      // get the "id " param string, conver string to a number using the "+" symbol
      this.currentCategoryId = +this.route.snapshot.paramMap.get('id');
      this.currentCategoryName = this.route.snapshot.paramMap.get('name');
    }else {
      this.currentCategoryId = 1;
    }

    // tslint:disable-next-line:triple-equals
    if (this.previousCategoryId != this.currentCategoryId) {
      this.pageNumber = 1;
    }
    this.previousCategoryId = this.currentCategoryId;
    console.log(`currentCategoryId=${this.currentCategoryId}, pageNumber=${this.pageNumber}`);



    // page number in the Spring start from 0, in angular from 1. So we need to sumbstract
    this.productService.getProductListPaginate(this.pageNumber - 1,
                                               this.pageSize,
                                               this.currentCategoryId).subscribe(this.processResult());
  }
  // tslint:disable-next-line:typedef
  updatePageSize(value: any){
    this.pageSize = value;
    this.pageNumber = 1;
    this.listProducts();
  }

  // tslint:disable-next-line:typedef
  processResult(){
    return data => {
      this.products = data._embedded.products;
      this.pageNumber = data.page.number + 1;
      this.pageSize = data.page.size;
      this.totalElements = data.page.totalElements;
    };
  }

  // tslint:disable-next-line:typedef
  addToCart(product: Product) {
    console.log(`Adding to cart: ${product.name}, ${product.unitPrice}`);

    const cartItem = new CartItem(product);

    this.cartService.addToCart(cartItem);
  }
}
