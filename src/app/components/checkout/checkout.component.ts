import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CartService} from '../../services/cart-service';
import {ShopFormService} from '../../services/shop-form.service';
import {Country} from '../../common/country';
import {County} from '../../common/county';
import {MyValidators} from '../../validators/my-validators';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  checkoutFormGroup: FormGroup;

  totalPrice = 0;
  totalQuantity = 0;

  creditCartYears: number[] = [];
  creditCartMonth: number[] = [];

  countries: Country[] = [];

  shippingAddressCounties: County[] = [];
  billingAddressCounties: County[] = [];

  constructor(private cartService: CartService, private formBuilder: FormBuilder, private shopFormService: ShopFormService) { }

  // tslint:disable-next-line:typedef
  get firstName() {return this.checkoutFormGroup.get('customer.firstName'); }

  // tslint:disable-next-line:typedef
  get lastName() {return this.checkoutFormGroup.get('customer.lastName'); }

  // tslint:disable-next-line:typedef
  get email() {return this.checkoutFormGroup.get('customer.email'); }

  ngOnInit(): void {

    this.checkoutFormGroup = this.formBuilder.group({
      customer: this.formBuilder.group({
        firstName: new FormControl('', [Validators.required, Validators.minLength(2), MyValidators.noWhiteSpaces]),
        lastName: new FormControl('', [Validators.required, Validators.minLength(2), MyValidators.noWhiteSpaces]),
        email: new FormControl('',
          [Validators.required,
          Validators.pattern('^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$')])
      }),
      shippingAddress: this.formBuilder.group({
        street: [''],
        city: [''],
        country: [''],
        county: [''],
        postcode: ['']
      }),
      billingAddress: this.formBuilder.group({
        street: [''],
        city: [''],
        country: [''],
        county: [''],
        postcode: ['']
      }),
      creditCard: this.formBuilder.group({
        cardType: [''],
        nameOnCard: [''],
        cardNumber: [''],
        securityCode: [''],
        expirationMonth: [''],
        expirationYear: ['']
      })
    });

    this.shopFormService.getCountries().subscribe(
      data => {
        console.log('Retrieved countries: ' + JSON.stringify(data));
        this.countries = data;
      }
    );

    const startMonth = new Date().getMonth() + 1;
    console.log('startMonth: ' + startMonth);
    this.shopFormService.getCreditCardMonths(startMonth).subscribe(
      data => {
        console.log('Retrieve credit card months: ' + JSON.stringify(data));
        this.creditCartMonth = data;
      }
    );

    // @ts-ignore
    this.shopFormService.getCreditCardYears().subscribe(
      data => {
        console.log('Retrieve credit card years: ' + JSON.stringify(data));
        this.creditCartYears = data;
      }
    );

    this.cartService.totalPrice.subscribe(
      data => this.totalPrice = data
    );

    this.cartService.totalQuantity.subscribe(
      data => this.totalQuantity = data
    );

    this.cartService.computeCartTotals();
  }

  // tslint:disable-next-line:typedef
  onSubmit(){
    console.log('Handling the submit button');

    if (this.checkoutFormGroup.invalid){
      this.checkoutFormGroup.markAllAsTouched();
      let scrollToTop = window.setInterval(() => {
        let pos = window.pageYOffset;
        if (pos > 0) {
          window.scrollTo(0, pos - 20); // how far to scroll on each step
        } else {
          window.clearInterval(scrollToTop);
        }
      }, 10);
    }

    console.log(this.checkoutFormGroup.get('customer').value);
    console.log(this.checkoutFormGroup.get('customer').value.email);

    console.log('The shipping address country is ' + this.checkoutFormGroup.get('shippingAddress').value.country.name);
    console.log('The shipping address county is ' + this.checkoutFormGroup.get('shippingAddress').value.county.name);
  }

  // tslint:disable-next-line:typedef
  handleMonthAndYears() {
    const creditCardFormGroup = this.checkoutFormGroup.get('creditCard');

    const currentYear: number = new Date().getFullYear();
    const selectedYear: number = Number(creditCardFormGroup.value.expirationYear);

    let startMonth: number;

    if (currentYear === selectedYear) {
      startMonth = new Date().getMonth() + 1;
    }else {
      startMonth = 1;
    }

    this.shopFormService.getCreditCardMonths(startMonth).subscribe(
      data => {
        console.log('Retrieved credit card months: ' + JSON.stringify(data));
        this.creditCartMonth = data;
      }
    );
  }

  // tslint:disable-next-line:typedef
  getCounty(formGroupName: string) {
    const formGroup = this.checkoutFormGroup.get(formGroupName);
    const countryCode = formGroup.value.country.code;
    const countryName = formGroup.value.country.name;

    console.log(`${formGroupName} country code: ${countryCode}`);
    console.log(`${formGroupName} country code: ${countryName}`);
    this.shopFormService.getCounties(countryCode).subscribe(
      data => {
        if (formGroupName === 'shippingAddress'){
          this.shippingAddressCounties = data;
        }else {
          this.billingAddressCounties = data;
        }
        formGroup.get('county').setValue(data[0]);
      }
    );
  }

  copyShippingAddressToBillingAddress(event) {

    if (event.target.checked) {
      this.checkoutFormGroup.controls.billingAddress.setValue(this.checkoutFormGroup.controls.shippingAddress.value);
      this.billingAddressCounties = this.shippingAddressCounties;
    }else {
      this.checkoutFormGroup.controls.billingAddress.reset();
      this.billingAddressCounties = [];
    }
  }
}
