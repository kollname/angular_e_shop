import {CartItem} from '../common/cart-item';
import {Subject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cartItems: CartItem[] = [];

  constructor() {
  }

// Subject is subclass of Observable. We can programm events with it
  totalPrice: Subject<number> = new Subject<number>();
  totalQuantity: Subject<number> = new Subject<number>();

  // tslint:disable-next-line:typedef
  addToCart(cartItem: CartItem){

    let alreadyExistsInCart = false;
    // tslint:disable-next-line:prefer-const
    let existingCartItem: CartItem;

    if (this.cartItems.length > 0){
      existingCartItem = this.cartItems.find(tempCartItem => tempCartItem.id === cartItem.id);

      // tslint:disable-next-line:triple-equals
      alreadyExistsInCart = (existingCartItem != undefined);
    }
    if (alreadyExistsInCart){
      existingCartItem.quantity++;
    }else {
      this.cartItems.push(cartItem);
    }
    this.computeCartTotals();
  }
  // tslint:disable-next-line:typedef
  computeCartTotals(){
    let totalPriceValue = 0;
    let totalQuantityValue = 0;

    for (const currentCartItem of this.cartItems){
      // @ts-ignore
      totalPriceValue += currentCartItem.quantity * currentCartItem.unitPrice;
      totalQuantityValue += currentCartItem.quantity;
    }

    // publish the new values ... all subscribers will reveive the new data
    this.totalPrice.next(totalPriceValue);
    this.totalQuantity.next(totalQuantityValue);

    this.logCartDat(totalPriceValue, totalQuantityValue);
  }
  // tslint:disable-next-line:typedef
  decrementQuantity(cartItem: CartItem) {
    cartItem.quantity--;
    if (cartItem.quantity === 0){
      this.remove(cartItem);
    }else {
      this.computeCartTotals();
    }
  }

  // tslint:disable-next-line:typedef
  inclementQuantity(cartItem: CartItem) {
    this.cartItems.push(cartItem);
  }

  // tslint:disable-next-line:typedef
  remove(cartItem: CartItem){
    // tslint:disable-next-line:triple-equals
    const itemIndex = this.cartItems.findIndex(tempCartItem => tempCartItem.id == cartItem.id);
    if (itemIndex > -1){
      this.cartItems.splice(itemIndex, 1);
      this.computeCartTotals();
    }
  }

  // tslint:disable-next-line:typedef
  logCartDat(totalPriveValue: number, totalQuatityValue: number){
    console.log(`Total price value: ${totalPriveValue}, TotalQuantityValue: ${totalQuatityValue}`);
  }
}
