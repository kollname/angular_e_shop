import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Country} from '../common/country';
import {map} from 'rxjs/operators';
import {County} from '../common/county';

@Injectable({
  providedIn: 'root'
})
export class ShopFormService {
  private countriesUrl = 'http://localhost:8088/api/countries';
  private countiesUrl = 'http://localhost:8088/api/counties';

  constructor(private httpClient: HttpClient) { }

  getCountries(): Observable<Country[]>{
    return this.httpClient.get<GetResponseCountries>(this.countriesUrl).pipe(
      map(response => response._embedded.countries)
    );
  }

  getCounties(countryCode: string): Observable<County[]>{
    const searchCountyUrl = `${this.countiesUrl}/search/findByCountryCode?code=${countryCode}`;

    // @ts-ignore
    return this.httpClient.get<GetResponseCounties>(searchCountyUrl).pipe(
      map(response => response._embedded.counties)
    );
  }

  // @ts-ignore
  getCreditCardMonths(startMonth: number): Observable<number[]>{
    // tslint:disable-next-line:prefer-const
    let data: number[] = [];
    for (let month = startMonth; month <= 12; month++){
      data.push(month);
    }
    return of(data);
  }

  // @ts-ignore
  getCreditCardYears(): Observable<number[]>{
    // tslint:disable-next-line:prefer-const
    let data: number[] = [];

    // @ts-ignore
    const startYear: number = new Date().getFullYear();
    console.log('Current year: ' + startYear);
    const endYear: number = startYear + 10;

    for (let year = startYear; year <= endYear; year++){
      data.push(year);
    }
    return of(data);
  }
}
interface GetResponseCountries {
  _embedded: {
    countries: Country[];
  }
}
interface GetResponseCounties {
  _embedded: {
    counties: County[];
  }
}
